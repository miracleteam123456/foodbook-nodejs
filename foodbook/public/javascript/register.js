function setCookie(cname, cvalue, exmin) {
    var d = new Date();
    d.setTime(d.getTime() + (exmin*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function showLoading(isShow) {
    var body = $("body");
    if (isShow) body.addClass("loading");
    else body.removeClass("loading");
}

$("#form-register").submit(function(e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var url = "/users/register"; // the script where you handle the form input.
    var data = $("#form-register").serialize();
    console.log(data);
    showLoading(true);
    $.ajax({
        type: "POST",
        url: url,
        dataType:'json',
        data: $("#form-register").serialize(), // serializes the form's elements.
        success: function(data)
        {
            if(data.status != 200){
                showLoading(false);
                console.log(JSON.stringify(data.message)); // show response from the php script.
                $('.error-re').html(JSON.stringify(data.message));
            }else {
                // console.log(JSON.stringify(data)); // show response from the php script.
                $('.error-re').html('');
                // alert( (jQuery.parseJSON( JSON.stringify(data))).message.username);
                // location.href = "http://localhost:3000/views/home"
                calLogin((jQuery.parseJSON( JSON.stringify(data))).message.username,(jQuery.parseJSON( JSON.stringify(data))).message.password);
            }
        }
    });
});

function calLogin(username,password){
    var url = "/users/login"; // the script where you handle the form input.
    $.ajax({
        type: "POST",
        url: url,
        dataType:'json',
        data: {
            username:username,
            password:password
        },
        success: function(data)
        {
            {
                $('.error').html('');
                setCookie("data",JSON.stringify(data),15)
                location.href = "/views/home"
            }

        }
    });
}
//update avatar
$('#submit-upload').submit(function (e) {
    var data = new FormData($('#submit-upload')[0]);
    var filename = $('#avatar-upload').val();
    var file = $('#avatar-upload');
    var extension = filename.replace(/^.*\./, '');
    e.preventDefault();
    showLoading(true);
    if (extension != 'jpg' && extension != 'jpeg' && extension != 'png' && extension != 'gif') {
        alert('You must upload image!');
        $("body").removeClass("loading");
    } else if(file[0].files[0].size > 204800) {
        alert('File size must be under 200kB!');
        $("body").removeClass("loading");
    } else {
        $.ajax({
            type: 'post',
            url: '/files/upload',
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function (data) {
                showLoading(false);
                alert('Success Upload ');
                $('#ava-input').val(data.message.filename)
            }
        })
    }
});